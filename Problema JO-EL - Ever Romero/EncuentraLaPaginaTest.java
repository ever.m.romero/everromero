import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EncuentraLaPaginaTest {

	@Test
	public void testPublic() {
		
		assertEquals(EncuentraLaPagina.searchWeb("https://www.tutorialspoint.com/java/util/arraylist_/add.html", "add.html"), "Estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("https:__www.adeu.com_java/util-arraylist_remove.html", "remove.html"), "Estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("https:--www.hola.com-java/util-arraylist_-create.html", "create.html"), "Estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("https:/www.adeu.com_java/util-array-list_remove.html", "util-array-list_remove.html"), "Estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("https://www.tutorialspoint.com/java/util/arraylist_/add.html", "create.html"), "No estas en la pagina");
		
	}
	
	@Test
	public void testPrivate() {
		
		assertEquals(EncuentraLaPagina.searchWeb("https://www.tutorialspoint.com/java/util/arraylist_add.html", "add.html"), "No estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("A5001eaejsl35D//holal.das-dfSd6", ""), "Estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("", ""), "Estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("", " "), "No estas en la pagina");
		assertEquals(EncuentraLaPagina.searchWeb("A5001-eaejsl/35D-holal.das_dfSd6_datd/contacto.html", "contacto.html"), "Estas en la pagina");

	}
	
}
