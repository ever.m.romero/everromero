import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class EncuentraLaPagina {

	static Scanner sc  = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		String url = sc.nextLine();
		
		String namePage = sc.nextLine();
		
		System.out.println(searchWeb(url, namePage));
	}
	
	public static String searchWeb(String url, String namePage) {
		
		String page = ""; 	
		String separator = "";
		
		int cntBarra = 0;
		int cntLinea = 0;
		int cntGuionBajo = 0;
		
		for(int i=0; i<url.length(); i++) {
			
			if(url.charAt(i) == '/') {
				
				cntBarra++;
			}
			
			if(url.charAt(i) == '-') {
				
				cntLinea++;
			}
			
			if(url.charAt(i) == '_') {
				
				cntGuionBajo++;
			}
		}
		
		if(cntBarra>cntGuionBajo && cntBarra>cntLinea) {
			
			separator = "/";
		
		}else if(cntLinea>cntGuionBajo && cntLinea>cntBarra) {
			
			separator = "-";
		
		}else if(cntGuionBajo>cntBarra && cntGuionBajo>cntLinea) {
			
			separator = "_";
		
		}else {
			
			separator = "/";
			
		}	
	
		String[] separateUrl = url.split(separator);
		
		ArrayList<String> listSeparateUrl = new ArrayList<>(Arrays.asList(separateUrl));
				
		for(int i=0; i<listSeparateUrl.size(); i++) {
			
			if(namePage.equals(listSeparateUrl.get(i))) {
	
				page = listSeparateUrl.remove(i);
			}
		}

		if(page.equals(namePage)) {
			
			return "Estas en la pagina";
		
		}else {
			
			return "No estas en la pagina";
		}
	}
	
}
