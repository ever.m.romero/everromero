import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

public class JavadocBuscaminas {

	static Scanner sc = new Scanner(System.in);
	static Random r = new Random();
	static int mines = 0;
	static String player="";
	static ArrayList<String> winners = new ArrayList<String>();
	
	public static void main(String[] args) {
		
		boolean out = false;
		int f = 0;
		int c = 0;
	
		while(!out) {
			
			int option = menu();
			switch(option) {
			
				case 1:
					System.out.println("Para empezar jugar tenes que ir a opciones, configurar tu juego y luego presionas el número de jugar."+"\r");
					System.out.println("Si sos de esos que no saben jugar al buscaminas (osea con solo dos neuronas funcionales) es facíl, los números que aparecen excepto el 9 son la cantidad de bombas adyacentes a esa casilla.");
					System.out.println("Ganas si descubrís todas las casillas donde no haya bombas y perdes si presionas una casilla con bomba. FIN"+"\r");
					
					System.out.println("(Aclaración: Si pulsaste en un mina aparecera un 4 indicando donde estaba la mina. En caso contrario el 4 seguirá significando la cantidad de bombas adyacentes)"+"\r");
					break;
				case 2:
					player = obtainPlayer();
					System.out.println("Escribe el número de filas del tablero");
					f = sc.nextInt();
					System.out.println("Escribe el número de columnas del tablero");
					c = sc.nextInt();
					System.out.println("Escribe el número de minas que habrá en el tablero");
					mines = sc.nextInt();
					break;
				case 3:
					play(player, f, c, mines);
					break;
				case 4:
					if(winners.isEmpty()) {
						
						System.out.println("Todavía no se registró ningun ganador..."+"\r");
					}else {
						
						System.out.println("Esta es la lista de ganadores COMO MESSIII"+"\r");
						System.out.println(winners.toString().replace("[", "").replace("]", ""));
					}
					
					break;
				case 0:
					out = true;
					break;
				 				
			}
		}
		
		System.out.println("Nos vemos wachim!! Merry Chritsmas");
		
	}

	/**
	 * Función que contiene ordenamente todas las funciones necesarias para la ejecución del juego.
	 * 
	 * @author Ever
	 * 
	 * @param player El usuario de la persona que va a jugar (String)
	 * @param f La cantidad de filas que habra en las dos matrices (int)
	 * @param c La cantidad de columnas que habra en las dos matrices (int)
	 * @param mines La cantidad de minas que habra en el juego (osea en la matriz de minas) (int)
	 */
	public static void play(String player, int f, int c, int mines) {
		
		int[][] matrixMines = new int[f][c];
		int[][] matrix = new int[f][c];
		boolean gameInProgress = false;
		
		initializeMines(matrixMines, mines, f, c);
		initializeMatrix(matrix);
		
		printMatrixInt(matrix);
		
		while(!gameInProgress) {

			int x = coordsX();
			int y = coordsY();
			
			if(!estoyDentro(matrix, x, y)) {
				
				System.out.println("ESTAS JUGANDO FUERA DEL TABLERO SALAME");
			}
			
			if(estoyDentro(matrix, x, y) && matrix[x][y] != 9) {
				
				System.out.println("Esa casilla ya está revelada ciego");
			}
			
			discover(matrix,matrixMines,x,y);
			
			gameInProgress = gameOver(matrix, matrixMines, x, y);
			
			printMatrixInt(matrix);		
			
		}
		
	
		
	}
	
	/**
	 * Función recursiva que descubre las casillas hasta encontrar una mina adyacente y coloca el número de la cantidad de minas de alrededor de la casilla.
	 * 
	 * @author Ever
	 * 
	 * @param matrix La matrix publica para el jugador (int[][])
	 * @param matrixMines La matrix que contiene las minas (int[][])
	 * @param x La fila de la casilla elegida (int)
	 * @param y La columna de la casilla elegida (int)
	 */ 
	public static void discover(int[][] matrix,int[][] matrixMines,int x,int y) {
		
		if(!estoyDentro(matrixMines, x, y)) return;
		if(matrix[x][y]!=9) return;
		
		while(true) {
			
			int number = uncover(matrixMines,x,y);
			
			if (number == 0) {
				matrix[x][y]=0;
				for(int i=x-1; i<=x+1; i++){
					for(int j=y-1; j<=y+1; j++) {
						discover(matrix, matrixMines, i, j);
					}
				}
				break;
			}
			else {
				matrix[x][y] = number;
				break;
			}
			
		}
		
		
	}
	
	
	/**
	 * Función que descubre la casilla elegida y aumenta el contador para las casillas de alrededor.
	 * 
	 * @author Ever
	 * 
	 * @param matrixMines matriz con las minas (int[][])
	 * @param x La fila de la casilla elegida (int)
	 * @param y La columna de la casilla elegida (int)
	 * @return El contador de las casillas de alrededor (int)
	 */
	public static int uncover(int[][] matrixMines, int x, int y) {

		
		int contador = 0;
				
		
		if(estoyDentro(matrixMines, x-1, y-1) && matrixMines[x-1][y-1] == 1) {
			
			contador++;
		}
		
		if(estoyDentro(matrixMines, x-1, y) && matrixMines[x-1][y] == 1) {
			
			contador++;
		}
		
		if(estoyDentro(matrixMines, x-1, y+1) && matrixMines[x-1][y+1] == 1) {
			
			contador++;
		}
		
		if(estoyDentro(matrixMines, x, y+1) && matrixMines[x][y+1] == 1) {
			
			contador++;
		}
		
		if(estoyDentro(matrixMines, x+1, y+1) && matrixMines[x+1][y+1] == 1) {
			
			contador++;
		}
		
		if(estoyDentro(matrixMines, x+1, y) && matrixMines[x+1][y] == 1) {
			
			contador++;
		}
		
		if(estoyDentro(matrixMines, x+1, y-1) && matrixMines[x+1][y-1] == 1) {
			
			contador++;
		}
		
		if(estoyDentro(matrixMines, x, y-1) && matrixMines[x][y-1] == 1) {
			
			contador++;
		}
		
		 return contador;
	}

	
	/**
	 * Función que mira si en la casilla elegida hay una mina, si hay una mina el juego termina y perdes.
	 * 
	 * @author Ever
	 * 
	 * @param matrix La matrix publica para el jugador (int[][])
	 * @param matrixMines La matrix que contiene las minas (int[][])
	 * @param x La fila de la casilla elegida (int)
	 * @param y La columna de la casilla elegida (int)
	 * @return Si habia una mina en la casilla elegida devuelve true, si no false (boolean)
	 */
	public static boolean gameOver(int[][] matrix, int[][] matrixMines, int x, int y) {
		
		
		int contador = 0;
		if(estoyDentro(matrixMines, x, y)) {
				
			if(matrixMines[x][y] == 1) {
				
				matrix[x][y] = 4;
				System.out.println("Perdiste como España Xd");
				return true;
			}
			
		}
			
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				
				
				if(matrix[i][j] == 9) {
					
					contador++;
				}
				
			}
		}
		
		if(contador == mines) {
			
			System.out.println("Ganaste como ARGENTINA PAPA");
			fiMatch();
			return true;
		}
		
		return false;
		
	}
	
	
	/**
	 * Función que agrega un jugador a la lista de ganadores si se gano la partida.
	 * 
	 * @author Ever
	 * 
	 */
	public static void fiMatch() {
		
		winners.add(player);
	}
	
	
	/**
	 * Función que inicializa minas dentro de una matriz.
	 * 
	 * @author Ever
	 * 
	 * @param matrixMines La matriz en la queramos inicializar las minas (int[][])
	 * @param mines El número de minas ha inicializar (int)
	 * @param f	El número de la fila (int).
	 * @param c El número de la columna (int).
	 */
	public static void initializeMines(int[][]matrixMines, int mines, int f, int c) {
		
		for (int i = 0; i < matrixMines.length; i++) {
			for (int j = 0; j < matrixMines[0].length; j++) {
				
				matrixMines[i][j] = 0;
				
			}
		}
		
		for (int j2 = 0; j2 < mines; j2++) {
			
			matrixMines[r.nextInt(0, f)][r.nextInt(0, c)] = 1;
			
		}
		
	}
	
	
	/**
	 * Función que recibe un numero por consola como coordenada X.
	 * 
	 * @author Ever
	 * 
	 * @return El número indicado por consola para la coordenada X (int)
	 */
	public static int coordsX() {
		
		System.out.println("Indica el número X");
		int numberX = sc.nextInt();
		
		
		return numberX;
	}
	
	/**
	 * Función que recibe un numero por consola como coordenada Y.
	 * 
	 * @author Ever
	 * 
	 * @return El número indicado por consola para la coordenada Y (int)
	 */
	public static int coordsY() {
		
		System.out.println("Indica el número Y");
		int numberY = sc.nextInt();
		
		return numberY;
	}
	
	/**
	 * Función que inicializa una matriz.
	 * 
	 * @author Ever
	 * 
	 * @param matrix La matriz que queramos inicializar (int[][])
	 */
	public static void initializeMatrix(int[][]matrix) {
		
		fillMatrix(matrix);
		
			
	}
	
	
	/**
	 * Función que llena una matriz de nueves.
	 * 
	 * @author Ever
	 * 
	 * @param matrix La matriz que queramos rellenar (int[][])
	 */
	public static void fillMatrix(int[][] matrix) {
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				
				matrix[i][j] = 9;
			}
		}
	}
	
	
	/**
	 * Función que verifica si estas dentro de una matriz o no.
	 * 
	 * @author Ever
	 * 
	 * @param mat La matriz que queramos verificar (int[][])
	 * @param f La posición de la fila dentro de la matriz (int)
	 * @param c La posición de la columna dentro de la matriz (int)
	 * @return Si esta dentro de la matriz devuelve true y si no false (boolean)
	 */
	public static boolean estoyDentro(int[][] mat, int f, int c) {
		return (f>=0 && c>=0  && f<mat.length && c<mat[0].length);
	}
	

	
	/**
	 * Función que imprime una matriz de ints.
	 * 
	 * @author Ever
	 * 
	 * @param mat La matriz que queramos imprimir (int[][])
	 */
	public static void printMatrixInt(int[][] mat) {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				System.out.print(mat[i][j]+" ");
			}System.out.println();
		}
	}

	
	
	/**
	 * Función que imprime el menu principal del juego.
	 * 
	 * @author Ever
	 * 
	 * @return La opción elegida por consola (int)
	 */
	public static int menu() {
		
		System.out.println("***** Buscaminas Menu Principal xdxdxd *****");
		System.out.println("Elegí una opción");
		System.out.println("1- Ver las instrucciones");
		System.out.println("2- Opciones");
		System.out.println("3- Jugar");
		System.out.println("4- Lista de ganadores NASHE");
		System.out.println("0- Salir");
		int opcion = sc.nextInt();
		sc.nextLine();
		return opcion;		
	}
	
	/**
	 * Función que devuelve el nombre pasado por consola.
	 * 
	 * @author Ever
	 * 
	 * @return El nombre escrito (String)
	 */
	public static String obtainPlayer() {

		String name;
		System.out.println("Indica tu nickname");
		name = sc.nextLine();
		return name;
	}
	

}